﻿using System;

namespace Video_07
{
    class Program
    {
        static void Main(string[] args)
        {   
            //Some operator examples         
            var a = 3;
            var b = 6;

            if (a > b )
            {
                // value a is greater than b
            }

            if (a < b )
            {
                // value a is smaller than b
            }

            if (a == b )
            {
                // value a is equal to b
            }
            
            var x = 5; //This is a statement
            1 + 2 / x //This is an expression

            Console.WriteLine("Expression or Statement?"); // This is a statement, only because of the semi-column at the end.

        }
    }
}
