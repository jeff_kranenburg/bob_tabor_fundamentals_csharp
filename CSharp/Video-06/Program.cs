﻿using System;

namespace Video_06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Jeff's Big Giveaway!");
            Console.Write("Choose a value 1, 2 or 3 => ");
            var userValue = Console.ReadLine();

            //Parse the value into a number
            bool isNumber = int.TryParse(userValue, out int answer);

            // if (isNumber) {
            //     if (answer == 1) {
            //         Console.WriteLine("Great you have have won a boat!");
            //     }
            //     else if (answer == 2) {
            //         Console.WriteLine("Great you have have won a car!");
            //     }
            //     else if (answer == 3) {
            //         Console.WriteLine("Great you have have won a trip to Europe!");
            //     }
            //     else {
            //         Console.WriteLine("No price sorry - please choose 1, 2 or 3");    
            //     }
            // }
            // else {
            //     Console.WriteLine("No price sorry - please choose 1, 2 or 3 (those are numbers :-)");    
            // }

            // conditional operator - Ternary operator
            var message = (isNumber) ? "You typed in a number" : "Yeah we know that was not a number";
            Console.WriteLine(message);

            //Printing Variables
            Console.Write("Message 01 - ");
            Console.Write("Message 02 - ");
            Console.Write("Message 03 ");
            Console.WriteLine(); //Empty Line
 
            //String Formatting
            var a = "Message 01";
            var b = "Message 02";
            var c = "Message 03";

            Console.WriteLine("{0} - {1} - {2}", a, b, c);

            //String Interpolation
            Console.WriteLine($"{a} - {b} - {c}");

        }
    }
}
