# Code for the C# Beginner Course

[Click here :-)](https://mva.microsoft.com/en-us/training-courses/c-fundamentals-for-absolute-beginners-16169)

# Creating a Solution file

The idea behind the solution file is that you can combine multiple projects inside a single solution. This allows you to open multiple projects inside a single instance of the IDE and you can select which project to launch by setting them as the starting project.

> Note: Without a solution file you can just open a project by double clicking on the `.csproj` file.

Create a solution file in the parent directory of the project:

`dotnet new sln -n <name>`

Add project to the solution

`dotnet sln <name>.sln add <folder>/<project>.csproj`

To see what projects are inside your solution 

`dotnet sln <name>.sln list`

More info: [https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-sln](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-sln)


# Changing project folder

When moving a project into a subfolder, you will need to change the path in `settings.json` and `tasks.json` file.


